// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  data() {
    return {
      logged: false
    };
  },
  router,
  template: '<App/>',
  components: {
    App
  },
  watch: {
    logged: function (newval, oldval) {
      console.log('watch', newval, oldval);
      if (newval === true && oldval === false) {
        this.$router.push({
          name: 'resume',
          params: {
            resumeid: 'material-dark'
          }
        });
      }

    }
  },
  mounted() {
    this.$on('logmein', () => {
      console.log('logging inn...');
      this.logged = true;
    });

    this.$router.beforeEach((to, from, next) => {

      console.log('to', to);
      console.log('from', from);

      console.log(to.matched.some(record => record.meta.requiresAuth));

      if (to.matched.some(record => record.meta.requiresAuth)) {
        console.log('requiresAuth');
        if (!this.logged) {
          return next({
            path: '/',
            query: {
              redirect: to.fullPath
            }
          });
        } else {
          return next();
        }
      } else {
        return next(); // make sure to always call next()!
      }

    });

  }
});
